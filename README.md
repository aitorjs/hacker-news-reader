# UIzard - Hacker news reader

This is a solution to the [Hacker news reader](https://uizard.notion.site/uizard/Quest-Front-end-Developer-89b58e89f4434634a022031b38cdcfaf).  

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)

## Overview

### The challenge

You’re building a simple Hacker News ([https://news.ycombinator.com](https://news.ycombinator.com/)) clone. The app will fetch data from a public JSON API endpoint and display it in a simple viewer.

The viewer should have two panels; a left pane showing the top 10 stories; and a right panel displaying a preview of the selected story using an iframe. When a user clicks a story in the left panel, the right panel should switch to the selected story.

Please refer to this simple interactive mockup made in Uizard to illustrate this: https://app.uizard.io/p/de328bd3

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [https://gitlab.com/aitorjs/hacker-news-reader](https://gitlab.com/aitorjs/hacker-news-reader)
- Live Site URL: [https://hacker-news-reader-git-master-aitorjs.vercel.app/](https://hacker-news-reader-git-master-aitorjs.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- Grid
- Flexbox
- Typescript
- [TailwindCSS](https://tailwindcss.org/) - CSS framework
- [Flowbyte](https://flowbite.com/) - TailwindCSS components
- [Standard](https://standardjs.com/) - JavaScript style guide, linter, and formatter
- [React](https://reactjs.org/) - JS library
- [Gitlab](https://gitlab.com/) - Git forks
- [Vercel](https://vercel.com/) - Frontend cloud

### What I learned

- Continue practising with CSS, flexbox, gridbox, tailwindcss, standard, typescript, reactjs, nextjs and deployment.

- First time using flowbite.

- Use of <Suspense> with fallback to present a scheleton while data is not loaded.

- Any feedback is so welcome! :)


