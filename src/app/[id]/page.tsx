import { getStory } from '../services/api'

export default async function IdStory({ params: { id } }: { params: { id: number } }) {
  const story = await getStory(id)
  return (
    <iframe className='w-full h-full' src={story.url} title={story.title} />
  )
}
