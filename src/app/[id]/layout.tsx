'use client'

import React from 'react'
import { useParams } from 'next/navigation'

export default function NewsLayout({
  children // will be a page or nested layout
}: {
  children: React.ReactNode
}) {
  const params = useParams()

  React.useEffect(() => {
    const list = document.getElementById('list') as HTMLInputElement
    list?.childNodes.forEach((d: any) => { d.style.backgroundColor = '' })
    const id = document.getElementById(String(params.id)) as HTMLInputElement
    id.style.backgroundColor = '#713f12'
  }, [params])

  return children
}
