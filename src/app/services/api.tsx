export async function getStories(): Promise<number[]> {
  return await fetch('https://hacker-news.firebaseio.com/v0/topstories.json?orderBy=%22$key%22&limitToFirst=5')
    .then(async r => await (r.json() as Promise<number[]>))
}

export async function getStory(id: number): Promise<{
  url: string, title: string, by: string
}> {
  return await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
    .then(async r => await (await r.json() as Promise<{ title: string, by: string, url: string }>))
}
