import { getStory } from '../services/api'

export async function ListItem({ id }: { id: number }) {
  const story = await getStory(id)
  return (
    <div className='flex flex-col gap-2 h-[64px]'>
      <p className='truncate'>{story.title}</p>
      <div className='flex items-center justify-between opacity-50'>
        <p>{story.by}</p>
        <p>Visit website {'>>'}</p>

      </div>
    </div>
  )
}
