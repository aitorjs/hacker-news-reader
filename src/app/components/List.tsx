'use client'

import { useParams } from 'next/navigation'

import { Suspense } from "react";
import { ListItem } from "./ListItem";
import { ListenItemLoading } from "./ListenItemLoading";
import Link from "next/link";
import React from 'react';

export function List({ storyes }: { storyes: any }) {

  const params = useParams()
  console.log('news layout', params.id)

  React.useEffect(() => {


  }, [params])

  return (
    <ul className='flex flex-col gap-4'>
      {storyes.map((id: number, x: number) => (
        <li key={id} id={String(id)} className={'px-2 py-4 ' + (x === 0 ? ' bg-yellow-900' : '')}>
          <Link href={`/${id}`}>
            <Suspense fallback={<ListenItemLoading />}>
              <ListItem id={id} key={id} />
            </Suspense>
          </Link>
        </li>
      ))}

    </ul>
  )
}