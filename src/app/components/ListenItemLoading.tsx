export function ListenItemLoading() {
  return (
    <div className='h-[64px] grid items-center'>
      <div role='status' className='w-full animate-pulse'>
        <div className='h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-full' />
        <span className='sr-only'>Loading...</span>
      </div>

      <div className='flex items-center justify-between'>
        <div role='status' className='w-full animate-pulse'>
          <div className='h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-12' />
        </div>
        <div role='status' className='w-full animate-pulse'>
          <div className='h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-full' />
        </div>
      </div>
    </div>
  )
}
